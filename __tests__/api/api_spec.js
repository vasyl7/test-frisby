const frisby = require('frisby');

it ('GET should return a status of 200 OK', function () {
  return frisby
    .get('https://jsonplaceholder.typicode.com/posts')
    .expect('status', 200);
});

it ('GET should return a status of 201 OK', function () {
  return frisby
    .setup({
      request: {
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      }
    })
    .post('https://jsonplaceholder.typicode.com/posts', {
      title: 'foo',
      body: 'bar'
    })
    .expect('status', 201);
});